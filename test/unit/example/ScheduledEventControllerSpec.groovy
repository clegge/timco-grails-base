package example



import grails.test.mixin.*
import spock.lang.*

@TestFor(ScheduledEventController)
@Mock(ScheduledEvent)
class ScheduledEventControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.scheduledEventInstanceList
            model.scheduledEventInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.scheduledEventInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def scheduledEvent = new ScheduledEvent()
            scheduledEvent.validate()
            controller.save(scheduledEvent)

        then:"The create view is rendered again with the correct model"
            model.scheduledEventInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            scheduledEvent = new ScheduledEvent(params)

            controller.save(scheduledEvent)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/scheduledEvent/show/1'
            controller.flash.message != null
            ScheduledEvent.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def scheduledEvent = new ScheduledEvent(params)
            controller.show(scheduledEvent)

        then:"A model is populated containing the domain instance"
            model.scheduledEventInstance == scheduledEvent
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def scheduledEvent = new ScheduledEvent(params)
            controller.edit(scheduledEvent)

        then:"A model is populated containing the domain instance"
            model.scheduledEventInstance == scheduledEvent
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/scheduledEvent/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def scheduledEvent = new ScheduledEvent()
            scheduledEvent.validate()
            controller.update(scheduledEvent)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.scheduledEventInstance == scheduledEvent

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            scheduledEvent = new ScheduledEvent(params).save(flush: true)
            controller.update(scheduledEvent)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/scheduledEvent/show/$scheduledEvent.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/scheduledEvent/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def scheduledEvent = new ScheduledEvent(params).save(flush: true)

        then:"It exists"
            ScheduledEvent.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(scheduledEvent)

        then:"The instance is deleted"
            ScheduledEvent.count() == 0
            response.redirectedUrl == '/scheduledEvent/index'
            flash.message != null
    }
}
