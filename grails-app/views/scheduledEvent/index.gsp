<%@ page import="example.ScheduledEvent" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'scheduledEvent.label', default: 'Scheduled Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container grailsDomainNav wrapperTop" role="navigation">
    <ul>
        <sec:ifAnyGranted roles="ROLE_ADMIN">
            <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></li>
        </sec:ifAnyGranted>
    </ul>
</div>

<div class="container ">
    <div class="row">
        <h1><g:message code="default.list.label" args="[entityName]"/></h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nisi sapien, pellentesque sit amet tempus in, ultrices at tellus. Curabitur ut lacus a libero accumsan aliquet. Phasellus molestie rutrum diam, in pulvinar arcu blandit in.</p>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
    </div>
</div>

<div class="container">
    <div class="row">
        <table class="table table-bordered table-striped dataTable" id="defaultDataTable">
            <thead>
            <tr>
                <th class="sortableHeader">Title</th>
                <th class="sortableHeader">Description</th>
                <th class="sortableHeader">Date</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${scheduledEventInstanceList}" status="i" var="scheduledEventInstance">
                <tr class="hover"
                    onclick='document.location = "<g:createLink action='show' id="${scheduledEventInstance.id}"/>" '>
                    <td>${fieldValue(bean: scheduledEventInstance, field: "title")}</td>
                    <td>${fieldValue(bean: scheduledEventInstance, field: "description")}</td>
                    <td class="text-nowrap"><g:formatDate format="MM/dd/yyyy"
                                                          date="${scheduledEventInstance.eventDate}"/></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>

<div class="container containerButtons">
    <div class="row">
        <g:form controller="scheduledEvent" action="downloadReport">
            <button name="download" type="submit" class=" btn btn-success btn-block buttonWithForm"
                    aria-label="Left Align" action="downloadReport">
                <span class="fa fa-download fa-lg" aria-hidden="true"> Export</span>
            </button>
        </g:form>
    </div>
</div>

</body>
</html>
