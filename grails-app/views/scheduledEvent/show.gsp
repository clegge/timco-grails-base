<%@ page import="example.ScheduledEvent" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'scheduledEvent.label', default: 'Scheduled Event')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<div class="container grailsDomainNav wrapperTop" role="navigation">
    <ul>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <sec:ifAnyGranted roles="ROLE_ADMIN">
            <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
        </sec:ifAnyGranted>
    </ul>
</div>

<div class="container ">
    <div class="row">
        <h1><g:message code="default.list.label" args="[entityName]"/></h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nisi sapien, pellentesque sit amet tempus in, ultrices at tellus. Curabitur ut lacus a libero accumsan aliquet. Phasellus molestie rutrum diam, in pulvinar arcu blandit in.</p>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
    </div>
</div>

<div class="container containerDisplay">
    <hr>
    <g:if test="${scheduledEventInstance?.title}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.title.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.title}
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.active}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.active.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.active}
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.eventType}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.eventType.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.eventType}
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.numberOfSpeakers}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.numberOfSpeakers.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.numberOfSpeakers}
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.admissionPrice}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.admissionPrice.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                <g:formatNumber number="${scheduledEventInstance.admissionPrice}" type="currency" currencyCode="USD"/>
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.description}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.description.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.description}
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.eventDate}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.eventDate.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                <g:formatDate format="yyyy-MM-dd" date="${scheduledEventInstance.eventDate}"/>
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.lastUpdated}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.lastUpdated.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.lastUpdated}
            </div>
        </div>
    </g:if>

    <g:if test="${scheduledEventInstance?.dateCreated}">
        <div class="row">
            <div class="col-xs-3 showLabel">
                <g:message code="scheduledEvent.dateCreated.label"/>
            </div>

            <div class="col-xs-9 displayValue">
                ${scheduledEventInstance.dateCreated}
            </div>
        </div>
    </g:if>
    <hr>
</div>
<sec:ifAnyGranted roles="ROLE_ADMIN">
    <div class="container containerButtons">
        <div class="row">
            <fieldset class="buttons">
                <g:form id="${scheduledEventInstance?.id}" controller="scheduledEvent" action="edit" method="post">
                    <g:hiddenField name="version" value="${scheduledEventInstance?.version}"/>
                    <button name="edit" class="btn btn-success btn-block buttonWithForm"
                            resource="${scheduledEventInstance}"
                            action="edit">
                        <span class="fa fa-edit fa-lg" aria-hidden="true"> Edit</span>
                    </button>
                </g:form>
                <g:form id="${scheduledEventInstance?.id}" controller="scheduledEvent" method="delete" action="delete">
                    <button name="delete" class=" btn btn-danger btn-block buttonWithForm" aria-label="Left Align"
                            resource="${scheduledEventInstance}" action="delete"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">

                        <span class="fa fa-remove fa-lg " aria-hidden="true"> Delete</span>
                    </button>
                </g:form>
            </fieldset>
        </div>
    </div>
</sec:ifAnyGranted>
</body>
</html>
