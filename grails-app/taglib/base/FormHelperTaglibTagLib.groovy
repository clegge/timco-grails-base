package base


class FormHelperTaglibTagLib {
    static namespace = "formHelper"
    /**
     * will spit out a focused form g.textfield if the page is of 'create'
     */
    def getFocus = {attrs ->
        def actionVal = attrs.actionValue
        attrs.remove("actionValue")
        if(actionVal.contains('create')){
            attrs.put("autofocus","true")
        }
        out << g.textField(attrs)
    }


}
