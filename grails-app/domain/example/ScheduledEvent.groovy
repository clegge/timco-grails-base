package example

class ScheduledEvent {
    Long id
    String title
    boolean active
    EventType eventType
    int numberOfSpeakers
    double admissionPrice
    String description
    Date eventDate
    Date lastUpdated
    Date dateCreated

    static constraints = {
        title blank: false, nullable: false, unique: false
        description blank: false, nullable: false, unique: false
        eventDate nullable: false, unique: false
        lastUpdated()
        dateCreated()
    }

    static mapping = {
        description type: 'text'
    }

    @Override
    String toString() {
        return title
    }
}